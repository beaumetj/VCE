# README

Se connecter au serveur notebook de l'université

http://jupyterhub.u-ga.fr

Ouvrir un nouveau terminal : `New` > `Terminal`

    mkdir depot_TP # create new folder
	cd depot_TP    # change directory ("cd") to this new folded
	git clone https://gricad-gitlab.univ-grenoble-alpes.fr/vischelt/VCE.git # clone the project

Ne jamais travailler dans `depot_TP`
Se créer un espace personnel de travail dans l’espace notebook et importer les énoncés-données-fonctions de TP dans cet espace personnel notebook :

    cd ~/notebooks # change directory to the notebooks folder
    cp -r ../depot_TP/VCE/TP_Sahel/ . # copy (cp) the TP_Sahel directory to here

Aller directement sur l'interface Jupyter et explorer, travailler, ...

# Installer les librairies manquantes

Il peut y avoir des librairies non installé de R (`maps` par exemple).
Pour les installer, il fait aller dans le terminal, lancer R 

    R
   
Puis les installer manuellement :

    > install.packages("maps")